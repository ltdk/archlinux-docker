DOCKER=docker
BUILDDIR=$(shell pwd)/build
OUTPUTDIR=$(shell pwd)/output

define rootfs
	mkdir -vp $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks
	find /usr/share/libalpm/hooks -exec ln -sf /dev/null $(BUILDDIR)/alpm-hooks{} \;

	mkdir -vp $(BUILDDIR)/var/lib/pacman/ $(OUTPUTDIR)
	install -Dm644 /usr/share/devtools/pacman-extra.conf $(BUILDDIR)/etc/pacman.conf
	cat pacman-conf.d-noextract.conf >> $(BUILDDIR)/etc/pacman.conf

	# we use user/group maps to fully preserve UID/GID without root
	unshare --map-root-user --map-auto -- \
		pacman -Sy \
			-r $(BUILDDIR) \
			--noconfirm \
			--dbpath $(BUILDDIR)/var/lib/pacman \
			--config $(BUILDDIR)/etc/pacman.conf \
			--noscriptlet \
			--hookdir $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks/ $(2)

	cp --recursive --preserve=timestamps --backup --suffix=.pacnew rootfs/* $(BUILDDIR)/

	unshare --map-root-user --map-auto -- chroot $(BUILDDIR) /usr/bin/update-ca-trust

	unshare --map-root-user --map-auto -- chroot $(BUILDDIR) bash -c ' \
		mkdir -p etc/pacman.d/gnupg && \
		pacman-key --init && \
		pacman-key --populate && \
		rm -rf etc/pacman.d/gnupg/{openpgp-revocs.d/,private-keys-v1.d/,pubring.gpg~,gnupg.S.}*\
	'

	ln -fs /usr/lib/os-release $(BUILDDIR)/etc/os-release

	# add system users
	unshare --map-root-user --map-auto -- chroot $(BUILDDIR) /usr/bin/systemd-sysusers --root "/"

	# remove passwordless login for root (see CVE-2019-5021 for reference)
	sed -i -e 's/^root::/root:!:/' "$(BUILDDIR)/etc/shadow"

	unshare --map-root-user --map-auto -- tar \
		--xattrs \
		--acls \
		--exclude-from=exclude \
		-C $(BUILDDIR) \
		-c . \
		-f $(OUTPUTDIR)/$(1).tar

	cd $(OUTPUTDIR); \
		zstd --long -T0 -8 $(1).tar; \
		sha256sum $(1).tar.zst > $(1).tar.zst.SHA256
endef

define dockerfile
	sed -e "s|TEMPLATE_ROOTFS_FILE|$(1).tar.zst|" \
	    -e "s|TEMPLATE_ROOTFS_RELEASE_URL|Local build|" \
	    -e "s|TEMPLATE_ROOTFS_DOWNLOAD|ROOTFS=\"$(1).tar.zst\"|" \
	    -e "s|TEMPLATE_ROOTFS_HASH|$$(cat $(OUTPUTDIR)/$(1).tar.zst.SHA256)|" \
	    -e "s|TEMPLATE_VERSION_ID|dev|" \
	    Dockerfile.template > $(OUTPUTDIR)/Dockerfile.$(1)
endef

.PHONY: clean
clean:
	if test -d $(BUILDDIR); then \
		find $(BUILDDIR) -type d -exec chmod 755 '{}' \; ; \
		find $(BUILDDIR) -type f -exec chmod 644 '{}' \; ; \
	fi
	rm -rf $(BUILDDIR) $(OUTPUTDIR)

$(OUTPUTDIR)/base.tar.zst:
	$(call rootfs,base,base)

$(OUTPUTDIR)/base-devel.tar.zst:
	$(call rootfs,base-devel,base base-devel)

$(OUTPUTDIR)/Dockerfile.base: $(OUTPUTDIR)/base.tar.zst
	$(call dockerfile,base)

$(OUTPUTDIR)/Dockerfile.base-devel: $(OUTPUTDIR)/base-devel.tar.zst
	$(call dockerfile,base-devel)

.PHONY: docker-image-base
image-base: $(OUTPUTDIR)/Dockerfile.base
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base -t archlinux/archlinux:base $(OUTPUTDIR)

.PHONY: docker-image-base-devel
image-base-devel: $(OUTPUTDIR)/Dockerfile.base-devel
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-devel -t archlinux/archlinux:base-devel $(OUTPUTDIR)
